const logger = require('../utilities/logger');
const axios = require('axios');
const CircularJSON = require('circular-json');
const commonService = require('./commonServices');

async function getDashboardSnapshot(userCode, sessionId) {
    let getiView = {
        'name': 'dbgsnap',
        'axpapp': 'easyconnect',
        'seed': '1983',
        's': process.env.S || sessionId,
        'sqlpagination': 'false',
        'params': {
            'pusername': userCode
        }
    };

    getiView = commonService.changeTransData(getiView);

    const getiview = {};
    const paramValue = {};
    const _parameter = [];

    getiview['getiview'] = getiView;
    _parameter.push(getiview);
    paramValue['_parameters'] = _parameter;
    logger.info(`getDashboardSnapshot paramValue ::: ${JSON.stringify(paramValue)}`);

    return new Promise((resolve, reject) => {
        axios
            .post(`${process.env.API_URL}ASBIViewRest.dll/datasnap/rest/TASBIViewREST/getiview`, JSON.stringify(paramValue))
            .then(
                result => {
                    return resolve(JSON.parse(JSON.stringify(CircularJSON.stringify(result.data))));
                }
            )
            .catch(
                reason => {
                    return reject(reason);
                }
            );
    });
}


async function getDashBoardTatTrend(userCode, sessionId) {
    let  getiView = {
        'name': 'dbgtat',
        'axpapp': 'easyconnect',
        'seed': '1983',
        's': process.env.S || sessionId,
        'sqlpagination': 'false',
        'params': {
            'pusername': userCode
        }
    };

    getiView = commonService.changeTransData(getiView);

    const getiview = {};
    const paramValue = {};
    const _parameter = [];

    getiview['getiview'] = getiView;
    _parameter.push(getiview);
    paramValue['_parameters'] = _parameter;
    logger.info(`getDashBoardTatTrend paramValue ::: ${JSON.stringify(paramValue)}`);

    return new Promise((resolve, reject) => {
        axios
            .post(`${process.env.API_URL}ASBIViewRest.dll/datasnap/rest/TASBIViewREST/getiview`, JSON.stringify(paramValue))
            .then(
                result => {
                    return resolve(JSON.parse(JSON.stringify(CircularJSON.stringify(result.data))));
                }
            )
            .catch(
                reason => {
                    return reject(reason);
                }
            );
    });
}

module.exports = DashBoardService = {
    getDashboardSnapshot,
    getDashBoardTatTrend,
};
