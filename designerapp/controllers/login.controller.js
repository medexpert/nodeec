const loginService = require('../services/loginServices');
const logger = require('../utility/logger');
const jwt = require('jsonwebtoken');
const masterService = require('../services/masterConfigService');
const commonServices = require('../services/commonServices');

const lgModule = {
    login: async(req, res, next) => {
        let errorMsg = 'Invalid Username or Password';

        try {
            const logObj = req.body;
            // const curSession = req.session;
            logger.info(`LOGIN API ::: ${JSON.stringify(logObj)}`);
            loginService.doAuthenticate(logObj.userName, logObj.passWord)
                .then(
                    result => {
                        const resultData = JSON.parse(result);
                        logger.info(`Response Here is : ${JSON.stringify(resultData)}`);

                        if (resultData.result[0].error) {
                            res.render('login', { errorMessage: 'Invalid Username and Password' });
                        } else {
                            logger.info(`Response::: ${JSON.stringify(resultData.result[0].result)}`);

                            const body = { data: logObj.userName };
                            const jwtToken = process.env.JWT_SECRET;
                            const token = jwt.sign(body, jwtToken, { expiresIn: '6h' });

                            let responsedata = {
                                jwtToken: token,
                                userProfiles: JSON.stringify(resultData.result[0].result),
                            };
                            logger.info(`Usergroup ::: ${resultData.result[0].result.ugroup}`);
                            if (!commonServices.isAuthorizedUser(resultData.result[0].result.ugroup)) {
                                errorMsg = 'You are not authorized to access this application';
                                res.render('login', { errorMessage: errorMsg });
                            }
                            var statusConfig = {
                                "onStarted": 2,
                                "onPaused": 11,
                                "onResumed": 2,
                                "onCompleted": 3,
                                "onReturned": 13
                            };
                            var ugroup = resultData.result[0].result.ugroup;

                            if (ugroup === 'IOSQC') {
                                statusConfig.onStarted = 2;
                                statusConfig.onPaused = 11;
                                statusConfig.onResumed = 2;
                                statusConfig.onCompleted = 3;
                                statusConfig.onReturned = 13;
                            } else if (ugroup === 'Designer') {
                                statusConfig.onStarted = 5;
                                statusConfig.onPaused = 11;
                                statusConfig.onResumed = 5;
                                statusConfig.onCompleted = 6;
                                statusConfig.onReturned = 13;
                            } else if (ugroup === 'Designer QC') {
                                statusConfig.onStarted = 8;
                                statusConfig.onPaused = 11;
                                statusConfig.onResumed = 8;
                                statusConfig.onCompleted = 9;
                                statusConfig.onReturned = 13;
                                statusConfig.onSubmitted = 10;
                            }

                            // switch (resultData.result[0].result.ugroup) {
                            //     case 'IOSQC':
                            //         statusConfig = {
                            //             "onStarted": 2,
                            //             "onPaused": 11,
                            //             "onResumed": 2,
                            //             "onCompleted": 3,
                            //             "onReturned": 13
                            //         };
                            //         break;
                            //     case 'Designer':
                            //         statusConfig = {
                            //             "onStarted": 5,
                            //             "onPaused": 11,
                            //             "onResumed": 5,
                            //             "onCompleted": 6,
                            //             "onReturned": 13
                            //         };
                            //         break;
                            //     case 'Designer QC':
                            //         statusConfig = {
                            //             "onStarted": 8,
                            //             "onPaused": 11,
                            //             "onResumed": 8,
                            //             "onCompleted": 9,
                            //             "onReturned": 13,
                            //             "onSubmitted": 10
                            //         };
                            //         break;
                            //     default:
                            //         statusConfig = {
                            //             "onStarted": 2,
                            //             "onPaused": 11,
                            //             "onResumed": 2,
                            //             "onCompleted": 3,
                            //             "onReturned": 13
                            //         };
                            // }
                            logger.info(`statusConfig for Usergroup ::: ${JSON.stringify(statusConfig)}`);
                            const envValue = {
                                "DEV_MODE": false,
                                "ENV_MODE": "env:var",
                                "API_URL": process.env.API_URL,
                                "STLVIEWER_URL": process.env.STLVIEWER_URL
                            };
                            masterService.getMasterConfiguration()
                                .then(
                                    result => {
                                        const resultData = JSON.parse(result);
                                        logger.info(`Master Config Data:: ${JSON.stringify(resultData)}`);
                                        res.render('authRedirect', { userProfiles: responsedata.userProfiles, jwtToken: responsedata.jwtToken, config: JSON.stringify(envValue), statusConfig: JSON.stringify(statusConfig), masterConfig: JSON.stringify(resultData) });
                                    }
                                )
                                .catch(
                                    reason => {
                                        const masterData = {
                                            result: [{
                                                data: {
                                                    designtypes: [{
                                                        designtype: ''
                                                    }],
                                                    products: {
                                                        productname: ''
                                                    },
                                                    reasons: [{
                                                        reason: ''
                                                    }]
                                                }
                                            }]
                                        };
                                        res.render('authRedirect', { userProfiles: responsedata.userProfiles, jwtToken: responsedata.jwtToken, config: JSON.stringify(envValue), statusConfig: JSON.stringify(statusConfig), masterConfig: JSON.stringify(JSON.stringify(masterData)) });
                                    }
                                );
                        }
                    }
                )
                .catch(
                    reason => {
                        logger.error(`Error LOGIN SERVICE ::: ${reason}`);
                        // curSession.errMessage = 'Invalid Username or Password';
                        res.render('login', { errorMessage: errorMsg });
                    }
                );
        } catch (err) {
            logger.error(`Error LOGIN API :::  ${err}`);
            res.render('login', { errorMessage: 'Invalid Username and Password' });
        }
    }
}
module.exports = lgModule;