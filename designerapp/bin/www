#!/usr/bin/env node

/**
 * Module dependencies.
 */

const app = require('../app');
const debug = require('debug')('thinclientapp:server');
const http = require('http');
const fs=require('fs');
const https = require('https');
const logger = require('../utility/logger');
const dbconfig = require('../db/dbconfig');
const oracledb = require("oracledb");
const privateKey  = fs.readFileSync(`${__dirname}/certificates/server.key`, 'utf8');
const certificate = fs.readFileSync(`${__dirname}/certificates/server.crt`, 'utf8');
const credentials = {key: privateKey, cert: certificate};

//oracledb.connectionClass = dbconfig.connectionClass;

oracledb.createPool({
  user:             dbconfig.user,
  password:         dbconfig.password,
  connectString:    dbconfig.connectString,
  poolMax:          44,
  poolMin:          2,
  poolIncrement:    5,
  poolTimeout:      4
}, function(err, pool) {

  if (err) {
    console.log("ERROR: ", new Date(), ": createPool() callback: " + err.message);
    return;
  }

  require('../db/oracledb.js')(pool);

});

/**
 * Get port from environment and store in Express.
 */

const port = normalizePort(process.env.PORT || '3019');
app.set('port', port);

/**
 * Create HTTP server.
 */
const httpsServer = https.createServer(credentials, app);
const httpServer = http.createServer(app);

const server = http.createServer(app);

if (process.env.NODE_ENV === 'production') {
  httpsServer.listen(port, function () {
    logger.info(`Port: ${port}`);
    logger.info(`Env: ${app.get("env")}`);
  });
} else {
  httpServer.listen(port, function () {
    logger.info(`Port: ${port}`);
    logger.info(`Env: ${app.get("env")}`);
  });
}
/**
 * Listen on provided port, on all network interfaces.
 */

/*
server.listen(port);
server.on('error', onError);
server.on('listening', onListening);
*/

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
  const port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  const bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
  const addr = server.address();
  const bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port;
  debug('Listening on ' + bind);
}
