const express = require('express');
const router = express.Router();
const passport = require('passport');
//All Controller imports
const lgModule = require('../controllers/login.controller');
const orderModule = require('../controllers/order.controller');
const widgetsController = require('../controllers/widget.controller');
const authmiddleware = require('../middlewares/auth.middleware');
/* GET home page. */
router.get('/', function(req, res, next) {
    res.redirect('/login');
});

router.get('/login', function(req, res, next) {
    res.render('login');
});

router.use('/app', lgModule.login);
router.use('/getWidgetData', authmiddleware.verifyToken, widgetsController.getWidgetdata);
router.use('/getqcreasons', authmiddleware.verifyToken, widgetsController.getqcreasons);
router.use('/newOrder', authmiddleware.verifyToken, orderModule.newOrder);
router.use('/updateCaseDetails', authmiddleware.verifyToken, orderModule.updateCaseDetails);
router.use('/searchOrder', authmiddleware.verifyToken, orderModule.searchOrderResult);
router.use('/updateStatus', authmiddleware.verifyToken, orderModule.updateStatus);
router.use('/updateStatusIosqc', authmiddleware.verifyToken, orderModule.updateStatusIosqc);
router.use('/updateStatusDesigner', authmiddleware.verifyToken, orderModule.updateStatusDesigner);
router.use('/updateStatusFinalQc', authmiddleware.verifyToken, orderModule.updateStatusFinalQc);

module.exports = router;