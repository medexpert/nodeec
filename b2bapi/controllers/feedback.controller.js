const logger = require('../utilities/logger');
const commonService = require('../services/commonServices');
const feedbackModule = {
    updateFeedback: async (req, res, next) => {
        try {
            const columnObj = req.body;
            const sessionId = req.headers.sessionid;
            const response = await commonService.updateFeedback(columnObj, sessionId);
            const responseData = response.data;
            if (responseData.result[0].error) {
                res.status(500).send('{"errorStatus":"Failed","message":"Unable to create new data"}');
            } else {
                res.status(200).send(responseData.result[0].message[0]);
            }
        } catch (err) {
            logger.error(`Error ${err}`);
            next(err);
        }
    }
}

module.exports = feedbackModule;
