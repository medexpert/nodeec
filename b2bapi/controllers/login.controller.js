const loginService = require('../services/loginService');
const logger = require('../utilities/logger');

const lgModule = {
    login: async (req, res, next) => {
        try {
            const logObj = req.body;

            logger.info(`JSON ::: ${JSON.stringify(logObj)}`);
            const response = await loginService.doAuthenticate(logObj.userName, logObj.passWord);

            const responseData = response.data;

            if (responseData.result[0].error) {
                res.status(401).send('{"errorStatus":"Failed","message":"Invalid username or Password"}');
            } else {
                logger.info(`Response::: ${JSON.stringify(responseData.result[0].result)}`);
                res.status(200).send(responseData.result[0].result);
            }
        } catch (err) {
            logger.error(`Error ${err}`);
            next(err);
        }
    }
}

module.exports = lgModule;
