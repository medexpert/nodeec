# Easy Connect (REST API)

This is a basic application to showcase how to use **Docker** and **Docker-Compose** to
build and run a frontend application.

### System requirements

- [Docker](https://www.docker.com/)
- [Docker-Compose](https://docs.docker.com/compose/overview/)

### Steps to Build and run the application

This is a backend REST API application using nodeJs.

#### Build node dependencies

We do not checkin **node_modules** into the git repo so you need to build the node
dependencies before running the application. We will use nodeJs Docker container
to build our dependencies.

We have added a file **npm.sh** to wrapup the docker container and run the npm
commands. Run the following command:

```
sh npm.sh npm install
```

This will download and install all the node modules specified in the
**package.json** file. It will also create **node_modules** folder in the directory.

### Build Docker container

Now we will build the Docker container and run our container. Run the following
commands:

```
docker-compose pull
```

The above command will pull all the container images mentioned in the
docker-compose file.

```
docker-compose build
```

The above command will build our frontend container using the Dockerfile located
at the root directory of our repository.

### Run the Docker container

```
docker-compose up
```


Open [localhost:8080](http://localhost:5000) and you should see the main landing
page.

Folder description

* app - entry point of express application
* validation - input validation
* config - environment specific files
* db - DB connection module
* migration - DB migration scripts
* route - express router middleware
* test - Unit test case pertaining to app
