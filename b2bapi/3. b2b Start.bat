ECHO "Installing Node Modules" 
CALL npm install

ECHO "Starting the application"
CALL pm2 start config/ecosystemb2b.config.js --env production && PAUSE
